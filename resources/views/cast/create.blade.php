@extends('layout.master')
@section('judul')
Tambah Cast
@endsection

@section('isi')
<form action="/cast" method="POST">
  @csrf
    <div class="form-group">
      <label >Nama</label>
      <input type="text" class="form-control" name="nama" placeholder="Masukkan Nama">
    </div>
    @error('nama')
        <div class="alert alert-danger">{{$message}} </div> 
    @enderror
    <div class="form-group">
        <label >Umur</label>
        <input type="number" class="form-control" name="umur" placeholder="Masukkan umur">
      </div>
      @error('umur')
        <div class="alert alert-danger">{{$message}} </div> 
    @enderror
      <div class="form-group">
        <label >Biografi</label>
        <textarea class="form-control" cols="30" rows="10" name="bio" ></textarea>
      </div>
      @error('bio')
      <div class="alert alert-danger">{{$message}} </div> 
  @enderror
    <button type="submit" class="btn btn-primary">Tambah</button>
  </form>
    
@endsection