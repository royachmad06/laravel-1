@extends('layout.master')
@section('judul')
Edit Cast {{$cast->nama}}
@endsection

@section('isi')
<form action="/cast/{{$cast->id}}" method="POST">
  @csrf
  @method('put')
    <div class="form-group">
      <label >Nama</label>
      <input type="text" value="{{$cast->nama}}" class="form-control" name="nama" placeholder="Masukkan Nama">
    </div>
    @error('nama')
        <div class="alert alert-danger">{{$message}} </div> 
    @enderror
    <div class="form-group">
        <label >Umur</label>
        <input type="number" value="{{$cast->umur}}" class="form-control" name="umur" placeholder="Masukkan umur">
      </div>
      @error('umur')
        <div class="alert alert-danger">{{$message}} </div> 
    @enderror
      <div class="form-group">
        <label >Biografi</label>
        <textarea class="form-control" cols="30" rows="10" name="bio" >{{$cast->bio}}</textarea>
      </div>
      @error('bio')
      <div class="alert alert-danger">{{$message}} </div> 
  @enderror
    <button type="submit" class="btn btn-primary">Simpan</button>
  </form>
    
@endsection