@extends('layout.master')
@section('judul')
Halaman Pendaftaran
@endsection
@section('isi')
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="{{url('/welcome')}}" method="post">
        @csrf
        <!-- text input -->
        <label for="firstname">First name:</label><br>
        <input type="text" id="firstname" name="firstname" value=""><br>
        <label for="lastname">Last name:</label><br>
        <input type="text" id="lastname" name="lastname" value="">
        <br><br>
        <!-- radio button  -->
        <label >Gender:</label><br>
            <input type="radio" id="male" name="gender" value="Male">Male<br>
            <input type="radio" id="female" name="gender" value="Female">Female<br>
            <input type="radio" id="other" name="gender" value="Other">Other
            
            <br><br>
        <!-- dropdown -->
        <label >Nationality:</label> <br>
            <select name="natiolity" id="nationality">
              <option value="Indonesian">Indonesian</option>
              <option value="Singapore">Singapore</option>
              <option value="Malaysian">Malayasian</option>
              <option value="Austrian">Austrian</option>
            </select>
            <br><br>
        <!-- checkbox -->
        <label >Language Spoken:</label> <br>
            <input type="checkbox" id="bahasa_indonesia" name="bahasa_indonesia" value="Bahasa Indonesia">Bahasa Indonesia<br>
            <input type="checkbox" id="bahasa_indonesia" name="english" value="English">English<br>
            <input type="checkbox" id="bahasa_indonesia" name="other" value="Other">Other<br>
            <br>
        <!-- textarea  -->
        <label for="Bio">Bio:</label> <br>
        <textarea name="bio" id="bio" cols="30" rows="10"></textarea> <br>
        <input type="submit" value="Submit">

      </form> 
@endsection